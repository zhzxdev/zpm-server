module.exports = {
  apps: [
    {
      name: 'ZPM Server',
      script: 'build/index.js',
      watch: false,
      env: {
        NODE_ENV: 'production'
      }
    }
  ]
}
