import S from 'fluent-json-schema'
import { FastifyPluginAsync } from 'fastify'
import { call } from '../../io/device'

const fn: FastifyPluginAsync = async (server) => {
  const reg = (name: string, minLevel = 1, schema: any = S.object()) => {
    server.post(
      `/device/:id/${name}`,
      {
        schema: {
          body: schema
        }
      },
      async (req) => {
        if (req.user.level < minLevel) throw server.httpErrors.forbidden()
        const { id } = <any>req.params
        return await call(name, id, <any>req.body)
      }
    )
  }
  const regGet = (name: string, minLevel = 1) => {
    server.get(`/device/:id/${name}`, async (req) => {
      if (req.user.level < minLevel) throw server.httpErrors.forbidden()
      const { id } = <any>req.params
      return await call(name, id, {})
    })
  }

  reg('ping', 0)

  regGet('screenshot', 1)
  reg('killps', 1)
  reg('startexplorer', 1)
  reg('relaunch', 1)
  reg('setpsd', 1, S.object().prop('enabled', S.boolean()).required())

  reg('exec', 127, S.object().prop('command', S.string()).required())
  reg('execbat', 127, S.object().prop('code', S.string()).required())
  reg('eval', 127, S.object().prop('code', S.string()).required())
  reg('setfloaticon', 127, S.object().prop('enabled', S.boolean()).required())
  reg('quit', 127)
  reg('appupgrade', 127)
  reg('closewindows', 127)
}

export default fn
